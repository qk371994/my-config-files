# important
HISTFILE=~/.cache/zsh/history 
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
export PATH="$HOME/.local/bin/scoria:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/share/music_fairy:$PATH"
export BOOST="/var/lib/jenkins/workspace/boost_1_59_0"
export FREETYPE="/var/lib/jenkins/workspace/freetype-2.6.3"

# prompt
#PS1='%F{blue}%1~>%f '
PS1='%F{blue}[%n@%m %F{red}%~]%f '

#stuff i added
pfetch
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias l='exa -al'
alias ll='exa -l'
alias rr='ranger'
alias cat='bat'
alias htop='btop -t'
alias top='btop -t'
alias ga='git add'
alias gc='git commit -m'
alias gp='git push'
alias cddoom='cd /home/hexis/.var/app/org.zdoom.GZDoom/.config/gzdoom'
#alias doom='flatpak run org.zdoom.GZDoom'
alias ex='tar -xvf'
alias comp='gcc scoria.c -o scoria ; ./scoria'
alias ed="ed -p 'ed:'"
alias power="auto-cpufreq"

if [ -e /home/hexis/.nix-profile/etc/profile.d/nix.sh ]; then . /home/hexis/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
