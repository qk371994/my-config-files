
#important
HISTFILE=~/.cache/zsh/history 
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

#prompt
PS1='%F{blue}%1~>%f '

#stuff i added
#neofetch
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias l='ls -al'
alias rebuild='sudo nixos-rebuild switch'
alias packages='sudo vim /etc/nixos/configuration.nix'
