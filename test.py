import os, time, subprocess, json

with open('config.json') as config_file:
    aliases = json.load(config_file)['aliases']

while True:
    inp = input("|: ")

    if inp == "":
        print("")
    else:
        # Split the input into a list of arguments
        #args = inp.split()

        # Run the command
        #subprocess.run(args)

        if inp in aliases:
            command = aliases[inp]
            subprocess.run(command.split())
            print("")
        else:
            subprocess.run(inp.split())
            print("")


#com = ["time", "hexfetch"]

#subprocess.run(com, text=True)
